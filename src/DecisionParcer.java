import java.util.Arrays;
import java.util.HashMap;


public class DecisionParcer {

    private final ValuesForParcing valuesForParcing;

    public DecisionParcer(ValuesForParcing valuesForParcing) {
        this.valuesForParcing = valuesForParcing;
    }

    public String[] polinomials(String[] args) {
        HashMap<String, String[]> parsed = new HashMap<>();
        Arrays.stream(args)
                .forEach(arg -> {
                    String[] keyAndValues = arg.split(valuesForParcing.keySeparator);
                    if (keyAndValues.length != 2) {
                        return;
                    }
                    parsed.put(keyAndValues[0], keyAndValues[1].split(valuesForParcing.valuesSeparator));
                });
        String[] values = parsed.get(valuesForParcing.defaultKey);
        if (values.length == 0) {
            ErrorPrinter.printKeyError(valuesForParcing.defaultKey);
        }
        Double reduce = Arrays.stream(values)
                .map(Double::parseDouble)
                .reduce(0.0, (acc, y) -> acc + 1.0 / y * 3);
        return values;
    }
}
