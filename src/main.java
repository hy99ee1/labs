import java.util.Arrays;
import java.util.HashMap;

public class main {
    public static void main(String[] args) {
        ValuesForParcing valuesForParcing = new ValuesForParcing("=",",","poly");
        DecisionParcer parser = new DecisionParcer(valuesForParcing);
        if (Arrays.stream(args).findAny().isEmpty()) {
            ErrorPrinter.printInputError();
            ErrorPrinter.printExample(valuesForParcing.defaultKey);
            System.exit(0);
        }
        String[] parsedArgs = parser.polinomials(args);
        System.out.println(Arrays.toString(parsedArgs));
    }

}
