public class ValuesForParcing {
    final String keySeparator;
    final String valuesSeparator;
    final String defaultKey;

    public ValuesForParcing(String keySeparator, String valuesSeparator, String defaultKey) {
        this.keySeparator = keySeparator;
        this.valuesSeparator = valuesSeparator;
        this.defaultKey = defaultKey;
    }
}
