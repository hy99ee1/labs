public class ErrorPrinter {

    static public void printExample(String key) {
        System.out.println("Пример аргументов коммандной строки " + "--" + key + " = 1,2,3,4,5");
    }
    static public void printInputError() {
        System.out.println("Входные данные не получены");
    }

    static public void printKeyError(String key) {
        System.out.println("Не найден ключ: "+ key);
    }

}
